module.exports = {
     entry: './src/core/shell.js',
     output: {
         path: './bin',
         filename: 'pageview.js',
         jsonpFunction: 'activengageJsonp'
     },
     resolve: {
         extensions: ['', '.js']
     }
 };